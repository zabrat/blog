import React, {useState} from 'react'
import PropTypes from 'prop-types'

import './customPaginator.scope.scss'

const CustomPaginator = ({
    itemsAmount,
    itemsPerPage,
}) => {

    const [currentPage, setCurrentPage] = useState(1);
    const pagesAmount = Math.ceil(itemsAmount / itemsPerPage)
    const pagesList = Array.from(Array(pagesAmount+1).keys()).slice(1)

    return(
        <div className='paginator-wrapper'>
            <div className='paginator-wrapper__pages-wrapper'>
                {pagesList.map(pageNumber => (
                    <button 
                        key={pageNumber}
                        onClick={() => setCurrentPage(pageNumber)}
                        className={`paginator-wrapper__page-number ${currentPage === pageNumber && 'current-page'}`
                    }>
                        {pageNumber}
                    </button>
                ))}
            </div>
        </div>
    )
}

CustomPaginator.propTypes = {
    itemsAmount: PropTypes.number.isRequired,
    itemsPerPage: PropTypes.number.isRequired,
}

CustomPaginator.defaultProps = {
    itemsAmount: 20,
    itemsPerPage: 4,
}

export default CustomPaginator;
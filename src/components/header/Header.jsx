import React, { useState } from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"
import { useStaticQuery, graphql } from "gatsby"
import "./header.scope.scss"

const Header = ({ siteTitle }) => {

  const { allDatoCmsPost } = useStaticQuery(
    graphql`
      query{
        allDatoCmsPost {
          tags: distinct(field: tag___title)
        }
      }
    `
  )

  const [ isTagsHidden, setIsTagsHidden ] = useState(true)


  return(
    <div className="header">
      <div className="header__info-wrapper">
        <Link to="/" className="header__site-title">
          {siteTitle}
        </Link>
        <button 
          onClick={() => setIsTagsHidden(!isTagsHidden)}
          className="header__tagsBtn"
        >
          Tags
        </button>
      </div>
      <div className={`header__tags-wrapper ${isTagsHidden && 'hidden'}`}>
          {
            allDatoCmsPost.tags.map(tag => (
              <Link to={`tags/${tag}`} className="header__tag">
                #{tag}
              </Link>
            ))
          }
        </div>
    </div>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

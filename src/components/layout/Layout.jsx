import React from "react"
import PropTypes from "prop-types"

import Header from "../header/Header"
import "./layout.scope.scss"

const Layout = ({ children }) => {
  return (
    <>
      <Header siteTitle="Blog" />
      <div className="main-container">
        <main>{children}</main>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout

import React from "react"
import { Link } from "gatsby"
import PropTypes from "prop-types"

import "./postCard.scope.scss"

const PostCard = ({ postData }) => {
  return (
    <Link to={`posts/${postData.slug}`} className="post-card">
      <div className="post-card__wrapper">
          <picture className="post-card__image-wrapper">
            <source srcSet={postData.postImage.medium.src} type="image/webp" media="(max-width: 1200px)"/>
            <source srcSet={postData.postImage.large.src} type="image/webp" media="(min-width: 1201px)"/>
            <img className="post-card__image" src={postData.postImage.fallback.src} alt={postData.tag.title}/>
          </picture>
          <p className="post-card__title">{postData.postTitle}</p> 
          <div className="post-card__description-container">
            <label 
              htmlFor={postData.id}
              onKeyDown={e => e.stopPropagation()}
              onClick={e => e.stopPropagation()}
              className="post-card__description-label"
            >
              Description
            </label>
              <input 
                onClick={e => e.stopPropagation()}
                id={postData.id}
                className="post-card__checkbox"
                type="checkbox"
                name='description'
            /> 
            <p className="post-card__description">{postData.description}</p> 
          </div>
          <div className="post-card__footer">
            <span className="post-card__tags">{postData.tag.title}</span>
            <div className="post-card__author-info">
              <img
                src={postData.postAuthor.avatar.url}
                alt={postData.postAuthor.authorName}
                className="post-card__author-image"
              />
              <span className="post-card__author-name">
                {postData.postAuthor.authorName}
              </span>
            </div>
          </div>
      </div>
    </Link>
    
  )
}

PostCard.propTypes = {
  postData: PropTypes.object.isRequired,
}

export default PostCard

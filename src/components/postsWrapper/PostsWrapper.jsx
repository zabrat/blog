import React, { useState } from "react"
import { useStaticQuery, graphql } from "gatsby"
import PostCard from "../postCard/PostCard"
import CustomPaginator from '../customPaginator/CustomPaginator'
import "./postsWrapper.scope.scss"

const PostsWrapper = () => {

  const [currentTag, setCurrentTag] = useState('all')

  const { allDatoCmsPost } = useStaticQuery(
    graphql`
      query{
        allDatoCmsPost{
          edges{
            node {
              id
              body
              slug
              tag{
                title
              }
              postTitle
              description
              postAuthor {
                authorName
                avatar {
                  url
                }
              }
              postImage {
                medium: fixed(height: 480, width: 640, imgixParams: {fm: "webp"}) {
                  src
                }
                large: fixed(height: 768, width: 1024, imgixParams: {fm: "webp"}) {
                  src
                }
                fallback: fixed(height: 480, width: 640, imgixParams: {fm: "jpg"}) {
                  src
                }
              }
            }
          }
          totalCount
        }
      }
    `
  )

  return (
    <>
      <div className="tags-dropdown-wrapper">
        <select 
          id="tags"
          name="tags" 
          onChange={event => setCurrentTag(event.target.value)}
          className="tags-dropdown" 
        >
          <option value="all">all</option>
          <option value="#america">#america</option>
          <option value="#sunflower">#sunflower</option>
          <option value="#city">#city</option>
        </select>
      </div>
      <div className="posts-wrapper">
        {allDatoCmsPost.edges.filter(postData => (
          currentTag === 'all' || postData.node.tags === currentTag
        )).map(postData => (
            <PostCard key={postData.node.id} postData={postData.node} />
        ))}
      </div>
      <CustomPaginator itemsAmount={allDatoCmsPost.totalCount}/>
    </>
  )
}

export default PostsWrapper

import React from "react"

import Layout from "../components/layout/Layout"
import SEO from "../components/seo"
import PostsWrapper from "../components/postsWrapper/PostsWrapper"

const IndexPage = () => {

  return(
    <Layout>
      <SEO title="Home" />
      <PostsWrapper />
    </Layout>
  )
}

export default IndexPage

import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/Layout"
import "./postPage.scope.scss"
import { GatsbyImage, getImage } from "gatsby-plugin-image"
import { Link } from "gatsby"

export default function PostPage({ data: { datoCmsPost } }) {

  const image = getImage(datoCmsPost.postAuthor.avatar)
  return (
    <Layout>
      <div className="post-page">
        <h1 className="post-page__title">{datoCmsPost.postTitle}</h1>
        <p className="post-page__text">
          <picture className="post-page__image-wrapper">
            <source srcSet={datoCmsPost.postImage.url} media="(min-width: 768px)"/>
            <img
              alt={datoCmsPost.postTitle}
              src={datoCmsPost.postImage.url}
              className="post-page__image"
            />
          </picture>
          {datoCmsPost.body}
        </p>
        <Link to={`tags/${datoCmsPost.tag.title}`}>
          <span className="post-card__tags">{datoCmsPost.tag.title}</span>
        </Link>
        <div className="post-page__author-info">
        <GatsbyImage image={image} alt={datoCmsPost.postAuthor.authorName} />
          <span className="post-page__author-name">
            {datoCmsPost.postAuthor.authorName}
          </span>
        </div>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query ($id: String) {
    datoCmsPost(id: { eq: $id }) {
      id
      body
      slug
      tag{
        title
      }
      postTitle
      postAuthor {
        authorName
        avatar {
          url
        }
      }
      postImage {
        url
      }
    }
  }
`
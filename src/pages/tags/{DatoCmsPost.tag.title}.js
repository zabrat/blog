import React from "react"
import { graphql } from "gatsby"
import Layout from "../../components/layout/Layout"
import { GatsbyImage, getImage } from "gatsby-plugin-image"

export default function TagPage({ data: { datoCmsPost } }) {

  const image = getImage(datoCmsPost.postAuthor.avatar)
  return (
    <Layout>
      <div>privet</div>
    </Layout>
  )
}

export const query = graphql`
  query ($id: String) {
    datoCmsPost(id: { eq: $id }) {
      id
      body
      slug
      tag {
        title
      }
      postTitle
      postAuthor {
        authorName
        avatar {
          url
        }
      }
      postImage {
        url
      }
    }
  }
`
import React from 'react'
import { graphql } from "gatsby"

export const PageQuery = graphql`
      query blogPageQuery($skip: Int!, $limit: Int!){
        allDatoCmsPost(
            limit: $limit
            skip: $skip
        ){
            edges{
            node {
                id
                body
                slug
                tag{
                title
                }
                postTitle
                description
                postAuthor {
                authorName
                avatar {
                    url
                }
                }
                postImage {
                url
                }
            }
            }
            totalCount
        }
    }`

const BlogList = ({ pageContext }) => {

    return (
        <div>{pageContext.currentPage}</div>
    )
}

export default BlogList;